# Translation of docs_krita_org_general_concepts___file_formats___file_svg.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_general_concepts___file_formats___file_svg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-19 03:24+0200\n"
"PO-Revision-Date: 2019-07-19 09:40+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.07.70\n"

#: ../../general_concepts/file_formats/file_svg.rst:1
msgid "The Scalable Vector Graphics file format in Krita."
msgstr "Формат файлів масштабованої векторної графіки у Krita."

#: ../../general_concepts/file_formats/file_svg.rst:10
msgid "SVG"
msgstr "SVG"

#: ../../general_concepts/file_formats/file_svg.rst:10
msgid "*.svg"
msgstr "*.svg"

#: ../../general_concepts/file_formats/file_svg.rst:10
msgid "Scalable Vector Graphics Format"
msgstr "Формат масштабованої векторної графіки"

#: ../../general_concepts/file_formats/file_svg.rst:15
msgid "\\*.svg"
msgstr "\\*.svg"

#: ../../general_concepts/file_formats/file_svg.rst:17
msgid ""
"``.svg``, or Scalable Vector Graphics, is the most modern vector graphics "
"interchange file format out there."
msgstr ""
"``.svg`` або Scalable Vector Graphics (масштабована векторна графіка) є "
"найсучаснішим форматом файлів для обміну даними векторної графіки."

#: ../../general_concepts/file_formats/file_svg.rst:19
msgid ""
"Being vector graphics, SVG is very light weight. This is because it usually "
"only stores coordinates and parameters for the maths involved with vector "
"graphics."
msgstr ""
"Оскільки для зберігання даних використовується векторна графіка, файли SVG є "
"дуже малими за розміром. Причина цього полягає у тому, що у файлах "
"зберігаються лише дані щодо координат та параметрів для формул, які "
"визначають вигляд векторної графіки."

#: ../../general_concepts/file_formats/file_svg.rst:21
msgid ""
"It is maintained by the W3C SVG working group, who also maintain other open "
"standards that make up our modern internet."
msgstr ""
"Супроводом стандарту займається робоча група з SVG W3C. Ця ж робоча група "
"працює над іншими відкритими стандартами, які лежать в основі сучасного "
"інтернету."

#: ../../general_concepts/file_formats/file_svg.rst:23
msgid ""
"While you can open up SVG files with any text-editor to edit them, it is "
"best to use a vector program like Inkscape. Krita 2.9 to 3.3 supports "
"importing SVG via the add shape docker. Since Krita 4.0, SVGs can be "
"properly imported, and you can export singlevector layers via :menuselection:"
"`Layer --> Import/Export --> Save Vector Layer as SVG...`. For 4.0, Krita "
"will also use SVG to save vector data into its :ref:`internal format "
"<file_kra>`."
msgstr ""
"Хоча файл у форматі SVG можна редагувати у будь-якому текстовому редакторі, "
"для редагування все ж варто користуватися спеціалізованими програмами для "
"редагування векторної графіки, зокрема Inkscape. У версіях Krita з 2.9 до "
"3.3 було передбачено імпортування SVG за допомогою бічної панелі додавання "
"форм. Починаючи з версії Krita 4.0, SVG можна імпортувати у звичний спосіб, "
"і ви можете експортувати окремі векторні шари за допомогою пункту меню :"
"menuselection:`Шар --> Імпортувати/Експортувати --> Зберегти векторний шар "
"як SVG...`. Починаючи з версії 4.0, Krita також використовує SVG для "
"зберігання шарів векторних даних всередині свого :ref:`власного формату "
"файлів <file_kra>`."

#: ../../general_concepts/file_formats/file_svg.rst:25
msgid ""
"SVG is designed for the internet, though sadly, because vector graphics are "
"considered a bit obscure compared to raster graphics, not a lot of websites "
"accept them yet. Hosting them on your own webhost works just fine though."
msgstr ""
"SVG було розроблено для інтернету. Втім, як це не прикро, оскільки векторна "
"графіка вважається дещо складнішою за растрову, дані у цьому форматі "
"приймаються в інтернеті не так широко, як цього хотілося б. Втім, не повинно "
"виникнути ніяких проблем зі зберіганням файлів у цьому форматі на вашому "
"власному сайті."
