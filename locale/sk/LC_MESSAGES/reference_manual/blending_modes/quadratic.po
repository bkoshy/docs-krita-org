# translation of docs_krita_org_reference_manual___blending_modes___quadratic.po to Slovak
# Roman Paholik <wizzardsk@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___blending_modes___quadratic\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-13 13:16+0200\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 18.12.3\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: ../../reference_manual/blending_modes/quadratic.rst:1
msgid ""
"Page about the quadratic blending modes in Krita: Freeze, Freeze-Reflect, "
"Glow, Glow-Heat, Heat, Heat-Glow, Heat-Glow/Freeze-Reflect Hybrid, Reflect "
"and Reflect-Freeze."
msgstr ""

#: ../../reference_manual/blending_modes/quadratic.rst:11
#: ../../reference_manual/blending_modes/quadratic.rst:15
msgid "Quadratic"
msgstr "Kvadratický"

#: ../../reference_manual/blending_modes/quadratic.rst:19
msgid ""
"The quadratic blending modes are a set of modes intended to give various "
"effects when adding light zones or overlaying shiny objects."
msgstr ""

#: ../../reference_manual/blending_modes/quadratic.rst:23
msgid "Freeze Blending Mode"
msgstr ""

#: ../../reference_manual/blending_modes/quadratic.rst:26
msgid "Freeze"
msgstr "Uzamknúť rozmery"

#: ../../reference_manual/blending_modes/quadratic.rst:28
msgid "The freeze blending mode. Inversion of the reflect blending mode."
msgstr ""

#: ../../reference_manual/blending_modes/quadratic.rst:33
msgid ""
".. image:: images/blending_modes/quadratic/"
"Blending_modes_Q_Freeze_Light_blue_and_Orange.png"
msgstr ""
".. image:: images/blending_modes/quadratic/"
"Blending_modes_Q_Freeze_Light_blue_and_Orange.png"

#: ../../reference_manual/blending_modes/quadratic.rst:33
msgid "Left: **Normal**. Right: **Freeze**."
msgstr ""

#: ../../reference_manual/blending_modes/quadratic.rst:39
msgid "Freeze-Reflect"
msgstr ""

#: ../../reference_manual/blending_modes/quadratic.rst:41
msgid "Mix of Freeze and Reflect blend mode."
msgstr ""

#: ../../reference_manual/blending_modes/quadratic.rst:46
msgid ""
".. image:: images/blending_modes/quadratic/"
"Blending_modes_Q_Freeze_Reflect_Light_blue_and_Orange.png"
msgstr ""
".. image:: images/blending_modes/quadratic/"
"Blending_modes_Q_Freeze_Reflect_Light_blue_and_Orange.png"

#: ../../reference_manual/blending_modes/quadratic.rst:46
msgid "Left: **Normal**. Right: **Freeze-Reflect**."
msgstr ""

#: ../../reference_manual/blending_modes/quadratic.rst:51
msgid "Glow"
msgstr "Žiara"

#: ../../reference_manual/blending_modes/quadratic.rst:53
msgid "Reflect Blend Mode with source and destination layers swapped."
msgstr ""

#: ../../reference_manual/blending_modes/quadratic.rst:58
msgid ""
".. image:: images/blending_modes/quadratic/"
"Blending_modes_Q_Glow_Light_blue_and_Orange.png"
msgstr ""
".. image:: images/blending_modes/quadratic/"
"Blending_modes_Q_Glow_Light_blue_and_Orange.png"

#: ../../reference_manual/blending_modes/quadratic.rst:58
msgid "Left: **Normal**. Right: **Glow**."
msgstr ""

#: ../../reference_manual/blending_modes/quadratic.rst:63
msgid "Glow-Heat"
msgstr ""

#: ../../reference_manual/blending_modes/quadratic.rst:65
msgid "Mix of Glow and Heat blend mode."
msgstr ""

#: ../../reference_manual/blending_modes/quadratic.rst:70
msgid ""
".. image:: images/blending_modes/quadratic/"
"Blending_modes_Q_Glow_Heat_Light_blue_and_Orange.png"
msgstr ""
".. image:: images/blending_modes/quadratic/"
"Blending_modes_Q_Glow_Heat_Light_blue_and_Orange.png"

#: ../../reference_manual/blending_modes/quadratic.rst:70
msgid "Left: **Normal**. Right: **Glow_Heat**."
msgstr ""

#: ../../reference_manual/blending_modes/quadratic.rst:75
msgid "Heat"
msgstr "Teplo"

#: ../../reference_manual/blending_modes/quadratic.rst:77
msgid "The Heat Blend Mode. Inversion of the Glow Blend Mode."
msgstr ""

#: ../../reference_manual/blending_modes/quadratic.rst:83
msgid ""
".. image:: images/blending_modes/quadratic/"
"Blending_modes_Q_Heat_Light_blue_and_Orange.png"
msgstr ""
".. image:: images/blending_modes/quadratic/"
"Blending_modes_Q_Heat_Light_blue_and_Orange.png"

#: ../../reference_manual/blending_modes/quadratic.rst:83
msgid "Left: **Normal**. Right: **Heat**."
msgstr ""

#: ../../reference_manual/blending_modes/quadratic.rst:88
msgid "Heat-Glow"
msgstr ""

#: ../../reference_manual/blending_modes/quadratic.rst:90
msgid "Mix of Heat, and Glow blending mode."
msgstr ""

#: ../../reference_manual/blending_modes/quadratic.rst:95
msgid ""
".. image:: images/blending_modes/quadratic/"
"Blending_modes_Q_Heat_Glow_Light_blue_and_Orange.png"
msgstr ""
".. image:: images/blending_modes/quadratic/"
"Blending_modes_Q_Heat_Glow_Light_blue_and_Orange.png"

#: ../../reference_manual/blending_modes/quadratic.rst:95
msgid "Left: **Normal**. Right: **Heat-Glow**."
msgstr ""

#: ../../reference_manual/blending_modes/quadratic.rst:100
msgid "Heat-Glow and Freeze-Reflect Hybrid"
msgstr ""

#: ../../reference_manual/blending_modes/quadratic.rst:102
msgid ""
"Mix of the continuous quadratic blending modes. Very similar to overlay, and "
"sometimes provides better result than overlay."
msgstr ""

#: ../../reference_manual/blending_modes/quadratic.rst:107
msgid ""
".. image:: images/blending_modes/quadratic/"
"Blending_modes_Q_Heat_Glow_Freeze_Reflect_Light_blue_and_Orange.png"
msgstr ""
".. image:: images/blending_modes/quadratic/"
"Blending_modes_Q_Heat_Glow_Freeze_Reflect_Light_blue_and_Orange.png"

#: ../../reference_manual/blending_modes/quadratic.rst:107
msgid "Left: **Normal**. Right: **Heat-Glow and Freeze-Reflect Hybrid**."
msgstr ""

#: ../../reference_manual/blending_modes/quadratic.rst:112
msgid "Reflect"
msgstr "Odraz"

#: ../../reference_manual/blending_modes/quadratic.rst:114
msgid ""
"Reflect is essentially Color Dodge Blending mode with quadratic falloff."
msgstr ""

#: ../../reference_manual/blending_modes/quadratic.rst:120
msgid ""
".. image:: images/blending_modes/quadratic/"
"Blending_modes_Q_Reflect_Light_blue_and_Orange.png"
msgstr ""
".. image:: images/blending_modes/quadratic/"
"Blending_modes_Q_Reflect_Light_blue_and_Orange.png"

#: ../../reference_manual/blending_modes/quadratic.rst:120
msgid "Left: **Normal**. Right: **Reflect**."
msgstr ""

#: ../../reference_manual/blending_modes/quadratic.rst:125
msgid "Reflect-Freeze"
msgstr ""

#: ../../reference_manual/blending_modes/quadratic.rst:127
msgid "Mix of Reflect and Freeze blend mode."
msgstr ""

#: ../../reference_manual/blending_modes/quadratic.rst:132
msgid ""
".. image:: images/blending_modes/quadratic/"
"Blending_modes_Q_Reflect_Freeze_Light_blue_and_Orange.png"
msgstr ""
".. image:: images/blending_modes/quadratic/"
"Blending_modes_Q_Reflect_Freeze_Light_blue_and_Orange.png"

#: ../../reference_manual/blending_modes/quadratic.rst:132
msgid "Left: **Normal**. Right: **Reflect-Freeze**."
msgstr ""
