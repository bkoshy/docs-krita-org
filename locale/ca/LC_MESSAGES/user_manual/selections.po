# Translation of docs_krita_org_user_manual___selections.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
# Josep Ma. Ferrer <txemaq@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: user_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-24 17:09+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.11.70\n"

# skip-rule: t-sp_2p
#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: clic esquerre del ratolí"

# skip-rule: t-sp_2p
#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: clic dret del ratolí"

# skip-rule: t-sp_2p
#: ../../<rst_epilog>:66
msgid ""
".. image:: images/icons/rectangular_select_tool.svg\n"
"   :alt: toolselectrect"
msgstr ""
".. image:: images/icons/rectangular_select_tool.svg\n"
"   :alt: eina de selecció rectangular"

# skip-rule: t-sp_2p
#: ../../<rst_epilog>:68
msgid ""
".. image:: images/icons/elliptical_select_tool.svg\n"
"   :alt: toolselectellipse"
msgstr ""
".. image:: images/icons/elliptical_select_tool.svg\n"
"   :alt: eina de selecció el·líptica"

# skip-rule: t-sp_2p
#: ../../<rst_epilog>:70
msgid ""
".. image:: images/icons/polygonal_select_tool.svg\n"
"   :alt: toolselectpolygon"
msgstr ""
".. image:: images/icons/polygonal_select_tool.svg\n"
"   :alt: eina de selecció poligonal"

# skip-rule: t-sp_2p
#: ../../<rst_epilog>:72
msgid ""
".. image:: images/icons/path_select_tool.svg\n"
"   :alt: toolselectpath"
msgstr ""
".. image:: images/icons/path_select_tool.svg\n"
"   :alt: eina de selecció de camins"

# skip-rule: t-sp_2p
#: ../../<rst_epilog>:74
msgid ""
".. image:: images/icons/outline_select_tool.svg\n"
"   :alt: toolselectoutline"
msgstr ""
".. image:: images/icons/outline_select_tool.svg\n"
"   :alt: eina de selecció de contorns"

# skip-rule: t-sp_2p
#: ../../<rst_epilog>:76
msgid ""
".. image:: images/icons/contiguous_select_tool.svg\n"
"   :alt: toolselectcontiguous"
msgstr ""
".. image:: images/icons/contiguous_select_tool.svg\n"
"   :alt: eina de selecció d'àrees contigües"

# skip-rule: t-sp_2p
#: ../../<rst_epilog>:78
msgid ""
".. image:: images/icons/similar_select_tool.svg\n"
"   :alt: toolselectsimilar"
msgstr ""
".. image:: images/icons/similar_select_tool.svg\n"
"   :alt: eina de selecció de colors similars"

#: ../../user_manual/selections.rst:1
msgid "How selections work in Krita."
msgstr "Com treballen les seleccions al Krita."

# skip-rule: t-acc_obe
#: ../../user_manual/selections.rst:1
msgid ":ref:`rectangle_selection_tool`"
msgstr ":ref:`rectangle_selection_tool`"

#: ../../user_manual/selections.rst:1
msgid "|toolselectrect|"
msgstr "|toolselectrect|"

#: ../../user_manual/selections.rst:1
msgid "Select the shape of a square."
msgstr "Selecciona la forma d'un quadrat."

# skip-rule: t-acc_obe
#: ../../user_manual/selections.rst:1
msgid ":ref:`ellipse_selection_tool`"
msgstr ":ref:`ellipse_selection_tool`"

#: ../../user_manual/selections.rst:1
msgid "|toolselectellipse|"
msgstr "|toolselectellipse|"

#: ../../user_manual/selections.rst:1
msgid "Select the shape of a circle."
msgstr "Selecciona la forma d'un cercle."

# skip-rule: t-acc_obe
#: ../../user_manual/selections.rst:1
msgid ":ref:`polygonal_selection_tool`"
msgstr ":ref:`polygonal_selection_tool`"

#: ../../user_manual/selections.rst:1
msgid "|toolselectpolygon|"
msgstr "|toolselectpolygon|"

#: ../../user_manual/selections.rst:1
msgid ""
"Click where you want each point of the Polygon to be. Double click to end "
"your polygon and finalize your selection area. Use the :kbd:`Shift + Z` "
"shortcut to undo last point."
msgstr ""
"Feu clic on vulgueu que estigui cada punt del Polígon. Feu doble clic per a "
"acabar el polígon i finalitzar la vostra àrea de selecció. Utilitzeu la "
"drecera :kbd:`Majús. + Z` per a desfer l'últim punt."

# skip-rule: t-acc_obe
#: ../../user_manual/selections.rst:1
msgid ":ref:`outline_selection_tool`"
msgstr ":ref:`outline_selection_tool`"

#: ../../user_manual/selections.rst:1
msgid "|toolselectoutline|"
msgstr "|toolselectoutline|"

#: ../../user_manual/selections.rst:1
msgid ""
"Outline/Lasso tool is used for a rough selection by drawing the outline."
msgstr ""
"L'eina Contorn/Llaç s'utilitza per a una selecció aproximada en dibuixar el "
"contorn."

# skip-rule: t-acc_obe
#: ../../user_manual/selections.rst:1
msgid ":ref:`similar_selection_tool`"
msgstr ":ref:`similar_selection_tool`"

#: ../../user_manual/selections.rst:1
msgid "|toolselectsimilar|"
msgstr "|toolselectsimilar|"

#: ../../user_manual/selections.rst:1
msgid "Similar Color Selection Tool."
msgstr "Eina de selecció de colors similars."

# skip-rule: t-acc_obe
#: ../../user_manual/selections.rst:1
msgid ":ref:`contiguous_selection_tool`"
msgstr ":ref:`contiguous_selection_tool`"

#: ../../user_manual/selections.rst:1
msgid "|toolselectcontiguous|"
msgstr "|toolselectcontiguous|"

#: ../../user_manual/selections.rst:1
msgid ""
"Contiguous or “Magic Wand” selects a field of color. Adjust the :guilabel:"
"`Fuzziness` to allow more changes in the field of color, by default limited "
"to the current layer."
msgstr ""
"Contigu o «Vareta màgica» selecciona un camp de color. Ajusta el :guilabel:"
"`Difuminat` per a permetre més canvis en el camp de color, de manera "
"predeterminada limitat a la capa actual."

# skip-rule: t-acc_obe
#: ../../user_manual/selections.rst:1
msgid ":ref:`path_selection_tool`"
msgstr ":ref:`path_selection_tool`"

#: ../../user_manual/selections.rst:1
msgid "|toolselectpath|"
msgstr "|toolselectpath|"

#: ../../user_manual/selections.rst:1
msgid ""
"Path select an area based on a vector path, click to get sharp corners or "
"drag to get flowing lines and close the path with the :kbd:`Enter` key or "
"connecting back to the first point."
msgstr ""
"El traç selecciona una àrea basada en un camí vectorial, feu clic per "
"obtenir cantonades més definides o arrossegueu per obtenir línies fluides i "
"tanqueu el traç amb la tecla :kbd:`Retorn` o torneu a connectar amb el "
"primer punt."

#: ../../user_manual/selections.rst:12
msgid "Selection"
msgstr "Selecció"

#: ../../user_manual/selections.rst:17
msgid "Selections"
msgstr "Seleccions"

#: ../../user_manual/selections.rst:19
msgid ""
"Selections allow you to pick a specific area of your artwork to change. This "
"is useful for when you want to move a section, transform it, or paint on it "
"without affecting the other sections. There are many selection tools "
"available that select in different ways. Once an area is selected, most "
"tools will stay inside that area. On that area you can draw or use gradients "
"to quickly get colored and/or shaded shapes with hard edges."
msgstr ""
"Les seleccions permeten seleccionar una àrea específica del vostre treball "
"artístic per a canviar-lo. Això és útil per a quan voleu moure una secció, "
"transformar-la o pintar-hi sense que afecti a les altres seccions. Hi ha "
"moltes eines de selecció disponibles que seleccionen de diferents maneres. "
"Una vegada se selecciona una àrea, la majoria de les eines romandran dins "
"d'aquesta àrea. En aquesta àrea podreu dibuixar o utilitzar degradats per "
"obtenir ràpidament formes amb color i/o ombres amb vores sòlides."

#: ../../user_manual/selections.rst:22
msgid "Creating Selections"
msgstr "Crear les seleccions"

#: ../../user_manual/selections.rst:24
msgid ""
"The most common selection tools all exist at the bottom of the toolbox. Each "
"tool selects things slightly differently. The links for each tool go into a "
"more detailed description of how to use it."
msgstr ""
"Les eines de selecció més comunes estan a la part inferior del quadre "
"d'eines. Cada eina selecciona les coses de manera lleugerament diferent. Els "
"enllaços per a cada eina s'inclouen en una descripció més detallada de com "
"emprar-la."

#: ../../user_manual/selections.rst:38
msgid ""
"You can also use the transform tools on your selection, a great way to try "
"different proportions on parts of your image."
msgstr ""
"També podeu utilitzar les eines de transformació sobre la vostra selecció, "
"una excel·lent manera de provar diferents proporcions sobre parts de la "
"vostra imatge."

#: ../../user_manual/selections.rst:41
msgid "Editing Selections"
msgstr "Editar les seleccions"

#: ../../user_manual/selections.rst:43
msgid ""
"The tool options for each selection tool gives you the ability to modify "
"your selection."
msgstr ""
"Les Opcions de l'eina per a cada eina de selecció permeten modificar la "
"vostra selecció."

#: ../../user_manual/selections.rst:47
msgid "Action"
msgstr "Acció"

#: ../../user_manual/selections.rst:47
msgid "Modifier"
msgstr "Modificador"

#: ../../user_manual/selections.rst:47
msgid "Shortcut"
msgstr "Drecera"

#: ../../user_manual/selections.rst:47
msgid "Description"
msgstr "Descripció"

#: ../../user_manual/selections.rst:49
msgid "Replace"
msgstr "Substitueix"

#: ../../user_manual/selections.rst:49
msgid "Ctrl"
msgstr "Ctrl"

#: ../../user_manual/selections.rst:49
msgid "R"
msgstr "R"

#: ../../user_manual/selections.rst:49
msgid "Replace the current selection."
msgstr "Substitueix la selecció actual."

#: ../../user_manual/selections.rst:51
msgid "Intersect"
msgstr "Interseca"

#: ../../user_manual/selections.rst:51
msgid "Shift + Alt"
msgstr "Majús. + Alt"

#: ../../user_manual/selections.rst:51 ../../user_manual/selections.rst:57
msgid "--"
msgstr "--"

#: ../../user_manual/selections.rst:51
msgid "Get the overlapping section of both selections"
msgstr "Obtenir la secció superposada d'ambdues seleccions."

#: ../../user_manual/selections.rst:53
msgid "Add"
msgstr "Afegeix"

#: ../../user_manual/selections.rst:53
msgid "Shift"
msgstr "Majús."

#: ../../user_manual/selections.rst:53
msgid "A"
msgstr "A"

#: ../../user_manual/selections.rst:53
msgid "Add the new selection to the current selection."
msgstr "Afegeix la selecció nova a la selecció actual."

#: ../../user_manual/selections.rst:55
msgid "Subtract"
msgstr "Sostreu"

#: ../../user_manual/selections.rst:55
msgid "Alt"
msgstr "Alt"

#: ../../user_manual/selections.rst:55
msgid "S"
msgstr "S"

#: ../../user_manual/selections.rst:55
msgid "Subtract the selection from the current selection."
msgstr "Sostreu la selecció de la selecció actual."

#: ../../user_manual/selections.rst:57
msgid "Symmetric Difference"
msgstr "Diferència simètrica"

#: ../../user_manual/selections.rst:57
msgid "Make a selection where both the new and current do not overlap."
msgstr "Crea una selecció, on tant la nova com l'actual no se superposaran."

#: ../../user_manual/selections.rst:61
msgid "You can change this in :ref:`tool_options_settings`."
msgstr "Podeu canviar-ho a :ref:`tool_options_settings`."

#: ../../user_manual/selections.rst:63
msgid ""
"If you hover over a selection with a selection tool and no selection is "
"activated, you can move it. To quickly go into transform mode, |mouseright| "
"and select :guilabel:`Edit Selection`."
msgstr ""
"Si passeu per sobre d'una selecció amb una eina de selecció i no s'activa "
"cap selecció, podreu moure-la. Per anar ràpidament al mode de transformació, "
"feu |mouseright| i seleccioneu :guilabel:`Edita la selecció`."

#: ../../user_manual/selections.rst:66
msgid "Removing Selections"
msgstr "Eliminar les seleccions"

#: ../../user_manual/selections.rst:68
msgid ""
"If you want to delete the entire selection, the easiest way is to deselect "
"everything. :menuselection:`Select --> Deselect`. Shortcut :kbd:`Ctrl + "
"Shift + A`."
msgstr ""
"Si voleu suprimir tota la selecció, la forma més senzilla és desseleccionar-"
"ho tot. :menuselection:`Selecciona --> Desselecciona`. Drecera :kbd:`Ctrl + "
"Majús. + A`."

#: ../../user_manual/selections.rst:71
msgid "Display Modes"
msgstr "Modes de visualització"

#: ../../user_manual/selections.rst:73
msgid ""
"In the bottom left-hand corner of the status bar there is a button to toggle "
"how the selection is displayed. The two display modes are the following: "
"(Marching) Ants and Mask. The red color with Mask can be changed in the "
"preferences. You can edit the color under :menuselection:`Settings --> "
"Configure Krita --> Display --> Selection Overlay`. If there is no "
"selection, this button will not do anything."
msgstr ""
"A la cantonada inferior esquerra de la barra d'estat hi ha un botó per "
"alternar com es mostra la selecció. Els dos modes de visualització són els "
"següents: (Marxant) Formigues i màscares. El color vermell amb màscara es "
"pot canviar a les preferències. Podeu editar el color sota :menuselection:"
"`Arranjament --> Configura el Krita --> Visualització --> Superposició de la "
"selecció`. Si no hi ha cap selecció, aquest botó no farà res."

#: ../../user_manual/selections.rst:77
msgid ".. image:: images/selection/Ants-displayMode.jpg"
msgstr ".. image:: images/selection/Ants-displayMode.jpg"

#: ../../user_manual/selections.rst:78
msgid ""
"Ants display mode (default) is best if you want to see the areas that are "
"not selected."
msgstr ""
"El mode de visualització Formigues (predeterminat) és millor si voleu veure "
"les àrees que no estan seleccionades."

#: ../../user_manual/selections.rst:81
msgid ".. image:: images/selection/Mask-displayMode.jpg"
msgstr ".. image:: images/selection/Mask-displayMode.jpg"

#: ../../user_manual/selections.rst:82
msgid ""
"Mask display mode is good if you are interested in seeing the various "
"transparency levels for your selection. For example, when you have a "
"selection with very soft edges due using feathering."
msgstr ""
"El mode de visualització Màscara és bo si esteu interessat en veure els "
"diferents nivells de transparència per a la vostra selecció. Per exemple, "
"quan teniu una selecció amb vores molt suaus a causa de l'ús de la selecció "
"suau."

#: ../../user_manual/selections.rst:86
msgid ""
"Mask mode is activated as well when a selection mask is the active layer so "
"you can see the different selection levels."
msgstr ""
"El mode màscara també s'activa quan una màscara de selecció és la capa "
"activa, de manera que veureu els diferents nivells de la selecció."

#: ../../user_manual/selections.rst:89
msgid "Global Selection Mask (Painting a Selection)"
msgstr "Màscara de selecció global (pintar una selecció)"

#: ../../user_manual/selections.rst:91
msgid ""
"The global Selection Mask is your selection that appears on the layers "
"docker. By default, this is hidden, so you will need to make it visible via :"
"menuselection:`Select --> Show Global Selection Mask`."
msgstr ""
"La màscara de selecció global és la vostra selecció que apareix sobre "
"l'acoblador Capes. De manera predeterminada, aquest resta ocult, per la qual "
"cosa l'haureu de fer visible mitjançant :menuselection:`Selecciona --> "
"Mostra la màscara de selecció global`."

#: ../../user_manual/selections.rst:94
msgid ".. image:: images/selection/Global-selection-mask.jpg"
msgstr ".. image:: images/selection/Global-selection-mask.jpg"

#: ../../user_manual/selections.rst:95
msgid ""
"Once the global Selection Mask is shown, you will need to create a "
"selection. The benefit of using this is that you can paint your selection "
"using any of the normal painting tools, including the transform and move. "
"The information is saved as grayscale."
msgstr ""
"Una vegada que es mostra la màscara de selecció global, haureu de crear una "
"selecció. L'avantatge d'utilitzar-ho, és que podreu pintar la vostra "
"selecció utilitzant qualsevol de les eines de pintura normals, incloses la "
"de transforma i mou. La informació es desarà com a escala de grisos."

# skip-rule: ff-enter
#: ../../user_manual/selections.rst:98
msgid ""
"You can enter the global selection mask mode quickly from the selection "
"tools by doing |mouseright| and select :guilabel:`Edit Selection`."
msgstr ""
"Podeu entrar ràpidament al mode màscara de selecció global des de les eines "
"de selecció fent |mouseright| i seleccionant :guilabel:`Edita la selecció`."

#: ../../user_manual/selections.rst:101
msgid "Selection from layer transparency"
msgstr "Seleccionar des de la transparència de la capa"

#: ../../user_manual/selections.rst:104
msgid ""
"You can create a selection based on a layer's transparency by right-clicking "
"on the layer in the layer docker and selecting :guilabel:`Select Opaque` "
"from the context menu."
msgstr ""
"Podeu crear una selecció basada en la transparència d'una capa fent clic "
"dret sobre la capa a l'acoblador Capes i seleccionant :guilabel:`Selecciona "
"l'opac` des del menú contextual."

#: ../../user_manual/selections.rst:108
msgid ""
"You can also do this for adding, subtracting and intersecting by going to :"
"menuselection:`Select --> Select Opaque`, where you can find specific "
"actions for each."
msgstr ""
"També podeu fer això per a sumar, sostreure i intersectar anant a :"
"menuselection:`Selecciona --> Selecciona l'opac`, on trobareu accions "
"específiques per a cadascuna."

#: ../../user_manual/selections.rst:110
msgid ""
"If you want to quickly select parts of layers, you can hold the :kbd:`Ctrl "
"+` |mouseleft| shortcut on the layer *thumbnail*. To add a selection do :kbd:"
"`Ctrl + Shift +` |mouseleft|, to remove :kbd:`Ctrl + Alt +` |mouseleft| and "
"to intersect :kbd:`Ctrl + Shift + Alt +` |mouseleft|. This works with any "
"mask that has pixel or vector data (so everything but transform masks)."
msgstr ""
"Si voleu seleccionar ràpidament parts de les capes, podeu mantenir premuda "
"la drecera :kbd:`Ctrl + feu` |mouseleft| sobre la *miniatura* de la capa. "
"Per afegir una selecció, premeu la drecera :kbd:`Ctrl + Majús. + feu` |"
"mouseleft|, per eliminar premeu :kbd:`Ctrl + Alt + feu` |mouseleft| i per a "
"intersectar premeu la drecera :kbd:`Ctrl + Majús. + Alt + feu` |mouseleft|. "
"Això funciona amb qualsevol màscara que tingui dades de píxels o vectorials "
"(per tant, totes menys amb les màscares de transformació)."

#: ../../user_manual/selections.rst:114
msgid "Pixel and Vector Selection Types"
msgstr "Tipus de selecció amb píxels i vectors"

#: ../../user_manual/selections.rst:116
msgid ""
"Vector selections allow you to modify your selection with vector anchor "
"tools. Pixel selections allow you to modify selections with pixel "
"information. They both have their benefits and disadvantages. You can "
"convert one type of selection to another."
msgstr ""
"Les seleccions amb vectors permeten modificar la vostra selecció amb les "
"eines d'ancoratge vectorial. Les seleccions amb píxels permeten modificar "
"les seleccions amb la informació dels píxels. Tots dos tenen els seus "
"avantatges i desavantatges. Podeu convertir un tipus de selecció a una altra."

#: ../../user_manual/selections.rst:119
msgid ".. image:: images/selection/Vector-pixel-selections.jpg"
msgstr ".. image:: images/selection/Vector-pixel-selections.jpg"

#: ../../user_manual/selections.rst:120
msgid ""
"When creating a selection, you can select what type of selection you want "
"from the Mode in the selection tool options: Pixel or Vector. By default "
"this will be a vector."
msgstr ""
"Quan creeu una selecció, podreu seleccionar quin tipus de selecció voleu des "
"del mode a les Opcions de l'eina per a la selecció: Píxels o Vectors. De "
"manera predeterminada aquest serà vectors."

#: ../../user_manual/selections.rst:122
msgid ""
"Vector selections can be modified as any other :ref:`vector shape "
"<vector_graphics>` with the :ref:`shape_selection_tool`, if you try to paint "
"on a vector selection mask it will be converted into a pixel selection. You "
"can also convert vector shapes to selection. In turn, vector selections can "
"be made from vector shapes, and vector shapes can be converted to vector "
"selections using the options in the :guilabel:`Selection` menu. Krita will "
"add a new vector layer for this shape."
msgstr ""
"Les seleccions amb vectors poden modificar-se com qualsevol altra :ref:"
"`forma vectorial <vector_graphics>` amb l':ref:`shape_selection_tool`, si "
"intenteu pintar sobre una màscara de selecció amb vectors, aquesta es "
"convertirà en una selecció amb píxels. També podeu convertir les formes "
"vectorials a la selecció. Al seu torn, les seleccions amb vectors es poden "
"crear a partir de formes vectorials i les formes vectorials es poden "
"convertir en seleccions amb vectors utilitzant les opcions en el menú :"
"guilabel:`Selecció`. El Krita afegirà una capa vectorial nova per a aquesta "
"forma."

#: ../../user_manual/selections.rst:124
msgid ""
"One of the most common reasons to use vector selections is that they give "
"you the ability to move and transform a selection without the kind of resize "
"artifacts you get with a pixel selection. You can also use the :ref:"
"`shape_edit_tool` to change the anchor points in the selection, allowing you "
"to precisely adjust bezier curves or add corners to rectangular selections."
msgstr ""
"Una de les raons més habituals per a utilitzar les seleccions amb vectors és "
"que us donen la capacitat de moure i transformar una selecció sense els "
"tipus de defectes de reamidar que s'obtenen amb una selecció amb píxels. "
"També podeu utilitzar l':ref:`shape_edit_tool` per a canviar els punts "
"d'ancoratge a la selecció, el qual us permetrà ajustar amb precisió les "
"corbes de Bézier o afegir cantonades a les seleccions rectangulars."

#: ../../user_manual/selections.rst:126
msgid ""
"If you started with a pixel selection, you can still convert it to a vector "
"selection to get these benefits. Go to :menuselection:`Select --> Convert to "
"Vector Selection`."
msgstr ""
"Si heu començat amb una selecció amb píxels, encara podeu convertir-la en "
"una selecció amb vectors per obtenir aquests avantatges. Aneu a :"
"menuselection:`Selecciona --> Converteix a selecció amb vectors`."

#: ../../user_manual/selections.rst:130
msgid ""
"If you have multiple levels of transparency when you convert a selection to "
"vector, you will lose the semi-transparent values."
msgstr ""
"Si teniu múltiples nivells de transparència quan convertiu una selecció en "
"vectorial, perdreu els valors semitransparents."

#: ../../user_manual/selections.rst:133
msgid "Common Shortcuts while Using Selections"
msgstr "Dreceres comuns mentre s'empren les seleccions"

#: ../../user_manual/selections.rst:135
msgid "Copy -- :kbd:`Ctrl + C` or :kbd:`Ctrl + Ins`"
msgstr "Copia: :kbd:`Ctrl + C` o :kbd:`Ctrl + Inser.`"

#: ../../user_manual/selections.rst:136
msgid "Paste -- :kbd:`Ctrl + V` or :kbd:`Shift + Ins`"
msgstr "Enganxa: :kbd:`Ctrl + V` o :kbd:`Majús. + Inser.`"

#: ../../user_manual/selections.rst:137
msgid "Cut -- :kbd:`Ctrl + X`, :kbd:`Shift + Del`"
msgstr "Retalla: :kbd:`Ctrl + X`, :kbd:`Majús. + Supr.`"

#: ../../user_manual/selections.rst:138
msgid "Copy From All Layers -- :kbd:`Ctrl + Shift + C`"
msgstr "Copia des de totes les capes: :kbd:`Ctrl + Majús. + C`"

#: ../../user_manual/selections.rst:139
msgid "Copy Selection to New Layer -- :kbd:`Ctrl + Alt + J`"
msgstr "Copia la selecció a una capa nova: :kbd:`Ctrl + Alt + J`"

#: ../../user_manual/selections.rst:140
msgid "Cut Selection to New Layer -- :kbd:`Ctrl + Shift + J`"
msgstr "Retalla la selecció a una capa nova: :kbd:`Ctrl + Majús. + J`"

#: ../../user_manual/selections.rst:141
msgid "Display or hide selection with :kbd:`Ctrl + H`"
msgstr "Mostra o oculta la selecció: :kbd:`Ctrl + H`"

#: ../../user_manual/selections.rst:142
msgid "Select Opaque -- :kbd:`Ctrl +` |mouseleft| on layer thumbnail."
msgstr ""
"Selecciona l'opac: :kbd:`Ctrl + feu` |mouseleft| sobre la miniatura de la "
"capa."

#: ../../user_manual/selections.rst:143
msgid ""
"Select Opaque (Add) -- :kbd:`Ctrl + Shift +` |mouseleft| on layer thumbnail."
msgstr ""
"Selecciona l'opac (afegeix): :kbd:`Ctrl + Majús. + feu` |mouseleft| sobre la "
"miniatura de la capa."

#: ../../user_manual/selections.rst:144
msgid ""
"Select Opaque (Subtract) -- :kbd:`Ctrl + Alt +` |mouseleft| on layer "
"thumbnail."
msgstr ""
"Selecciona l'opac (sostreu): :kbd:`Ctrl + Alt + feu` |mouseleft| sobre la "
"miniatura de la capa."

#: ../../user_manual/selections.rst:145
msgid ""
"Select Opaque (Intersect) -- :kbd:`Ctrl + Shift + Alt +` |mouseleft| on "
"layer thumbnail."
msgstr ""
"Selecciona l'opac (intersecta): :kbd:`Ctrl + Majús. + Alt + feu` |mouseleft| "
"sobre la miniatura de la capa."
