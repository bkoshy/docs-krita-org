# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-09 03:41+0200\n"
"PO-Revision-Date: 2019-05-23 18:52+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: toolshapeselection icons Krita image tool options en\n"
"X-POFile-SpellExtra: right images alt shapeselecttool Kritamouseleft blue\n"
"X-POFile-IgnoreConsistency: Type\n"
"X-POFile-SpellExtra: and selection selections geometry stroke shapes shape\n"
"X-POFile-SpellExtra: green mouseleft tools properties fill\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: botão esquerdo"

#: ../../<rst_epilog>:10
msgid ""
".. image:: images/icons/shape_select_tool.svg\n"
"   :alt: toolshapeselection"
msgstr ""
".. image:: images/icons/shape_select_tool.svg\n"
"   :alt: ferramenta de selecção de formas"

#: ../../reference_manual/tools/shape_selection.rst:None
msgid ""
".. image:: images/tools/shape-selection-menu-geometry.png\n"
"   :alt: Tool options: Geometry tool."
msgstr ""
".. image:: images/tools/shape-selection-menu-geometry.png\n"
"   :alt: Opções da ferramenta: Ferramenta de geometria."

#: ../../reference_manual/tools/shape_selection.rst:None
msgid ""
".. image:: images/tools/shape-selection-menu-stroke.png\n"
"   :alt: Tool options: Stroke tool."
msgstr ""
".. image:: images/tools/shape-selection-menu-stroke.png\n"
"   :alt: Opções da ferramenta: Ferramenta do traço."

#: ../../reference_manual/tools/shape_selection.rst:None
msgid ""
".. image:: images/tools/shape-selection-menu-fill.png\n"
"   :alt: Tool options: Fill tool."
msgstr ""
".. image:: images/tools/shape-selection-menu-fill.png\n"
"   :alt: Opções da ferramenta: Ferramenta de preenchimento."

#: ../../reference_manual/tools/shape_selection.rst:1
msgid "Krita's shape selection tool reference."
msgstr "A referência da ferramenta de selecção de formas."

#: ../../reference_manual/tools/shape_selection.rst:13
msgid "Tools"
msgstr "Ferramentas"

#: ../../reference_manual/tools/shape_selection.rst:13
msgid "Vector"
msgstr "Vector"

#: ../../reference_manual/tools/shape_selection.rst:13
msgid "Shape Selection"
msgstr "Selecção de Formas"

#: ../../reference_manual/tools/shape_selection.rst:18
msgid "Shape Selection Tool"
msgstr "Ferramenta de Selecção de Formas"

#: ../../reference_manual/tools/shape_selection.rst:20
msgid "|toolshapeselection|"
msgstr "|toolshapeselection|"

#: ../../reference_manual/tools/shape_selection.rst:22
msgid ""
"The shape selection tool used to be called the \"default\" tool. This had to "
"do with Krita being part of an office suite once upon a time. But this is no "
"longer the case, so we renamed it to its purpose in Krita: Selecting shapes! "
"This tool only works on vector layers, so trying to use it on a paint layer "
"will give a notification."
msgstr ""
"A ferramenta de selecção de formas costumava ser a ferramenta \"por omissão"
"\". Isto tinha a ver com o facto de o Krita antigamente fazer parte de um "
"pacote de escritório. Contudo, isto já não é mais o caso, como tal mudámos o "
"nome dela no Krita para esse fim: Selecção de formas! Esta ferramenta só "
"funciona com camadas vectoriais; como tal, tentar usá-la numa camada de "
"pintura irá gerar uma notificação."

#: ../../reference_manual/tools/shape_selection.rst:24
msgid ""
"After you create vector shapes, you can use this tool to select, transform, "
"and access the shape's options in the tool options docker. There are a lot "
"of different properties and things you can do with each vector shape."
msgstr ""
"Depois de criar formas vectoriais, poderá usar esta ferramenta para "
"seleccionar, transformar e aceder às opções da forma na área de opções da "
"ferramenta. Existem diversas propriedades e coisas que poderá fazer com cada "
"uma das formas vectoriais."

#: ../../reference_manual/tools/shape_selection.rst:27
msgid "Selection"
msgstr "Selecção"

#: ../../reference_manual/tools/shape_selection.rst:28
msgid "Selecting shapes can be done by two types of actions:"
msgstr "A selecção de formas pode ser feita através de dois tipos de acções:"

#: ../../reference_manual/tools/shape_selection.rst:30
msgid "|mouseleft| on a single shape to select it."
msgstr "Usando o |mouseleft| sobre uma única forma para a seleccionar."

#: ../../reference_manual/tools/shape_selection.rst:32
msgid ""
"*Blue selection* (drag left to right): selects only shapes fully covered."
msgstr ""
"*Selecção azul* (arraste da esquerda para a direita): selecciona apenas as "
"formas cobertas por completo."

#: ../../reference_manual/tools/shape_selection.rst:33
msgid "|mouseleft| and drag to select multiple shapes."
msgstr "Use o |mouseleft| e o arrastamento para seleccionar várias formas."

#: ../../reference_manual/tools/shape_selection.rst:33
msgid "*Green selection* (drag right to left): selects all the touched shapes."
msgstr ""
"*Selecção verde* (arrastamento da direita para a esquerda): selecciona todas "
"as formas tocadas."

#: ../../reference_manual/tools/shape_selection.rst:39
msgid ""
".. image:: images/tools/blue-and-green-selections.png\n"
"   :alt: Left: Blue selection. Right: Green selection."
msgstr ""
".. image:: images/tools/blue-and-green-selections.png\n"
"   :alt: Esquerda: Selecção azul. Direita: Selecção verde."

#: ../../reference_manual/tools/shape_selection.rst:39
msgid ""
"Blue selection: left-to-right, selects fully covered images. --  Green "
"selection: right-to-left, selects touched shapes."
msgstr ""
"Selecção azul: esquerda-para-direita, seleccione as imagens cobertas por "
"completo. -- Selecção verde: direita-para-esquerda, selecciona as formas "
"tocadas."

#: ../../reference_manual/tools/shape_selection.rst:42
msgid "Placement, Scale, Angle and Distortion"
msgstr "Colocação, Escala, Ângulo e Distorção"

#: ../../reference_manual/tools/shape_selection.rst:44
msgid ""
"Once an object is selected, a dashed bounding box will appear around it. The "
"box will also have square handles. You can use this bounding box to do "
"adjust: placement, scale, angle and distortion of the selected object."
msgstr ""
"Assim que tiver seleccionado um objecto, irá aparecer uma caixa tracejada em "
"torno do objecto. Essa caixa também irá ter pegas quadradas. Poderá usar "
"esta caixa envolvente para ajustar os seguintes parâmetros: colocação, "
"escala, ângulo e distorção do objecto seleccionado."

#: ../../reference_manual/tools/shape_selection.rst:50
msgid ""
".. image:: images/tools/shapes-selection-properties.png\n"
"   :alt: Left to right: Placement, Scale, Angle and Distortion."
msgstr ""
".. image:: images/tools/shapes-selection-properties.png\n"
"   :alt: Esquerda para a direita: Colocação, Escala, Ângulo e Distorção."

#: ../../reference_manual/tools/shape_selection.rst:50
msgid "Left to right: Placement, Scale, Angle and Distortion."
msgstr "Esquerda para a direita: Colocação, Escala, Ângulo e Distorção."

#: ../../reference_manual/tools/shape_selection.rst:52
msgid "Placement"
msgstr "Colocação"

#: ../../reference_manual/tools/shape_selection.rst:53
msgid ""
"|mouseleft| and hold inside the bounding box, while holding move the shape "
"to the desired position."
msgstr ""
"Use o |mouseleft| e mantenha-o carregado dentro da área envolvente; enquanto "
"mantém carregado, mova a forma para a posição desejada."

#: ../../reference_manual/tools/shape_selection.rst:54
msgid "Scale"
msgstr "Escala"

#: ../../reference_manual/tools/shape_selection.rst:55
msgid ""
"|mouseleft| and hold inside any of the square handles, move to adjust the "
"dimensions of the object."
msgstr ""
"Use o |mouseleft| e mantenha-o carregado dentro de qualquer uma das pegas "
"quadradas, mova para ajustar as dimensões do objecto."

#: ../../reference_manual/tools/shape_selection.rst:56
msgid "Angle"
msgstr "Ângulo"

#: ../../reference_manual/tools/shape_selection.rst:57
msgid ""
"Place the cursor slightly outside any of the corner handles. |mouseleft| and "
"drag to adjust the angle of the shape."
msgstr ""
"Coloque o cursor ligeiramente fora de qualquer uma das pegas dos cantos. Use "
"o |mouseleft| e arraste para ajustar o ângulo da forma."

#: ../../reference_manual/tools/shape_selection.rst:59
msgid "Distortion"
msgstr "Distorção"

#: ../../reference_manual/tools/shape_selection.rst:59
msgid ""
"Place the cursor slightly outside any of the middle handles. |mouseleft| and "
"drag to skew the shape."
msgstr ""
"Coloque o cursor ligeiramente fora de qualquer uma das pegas do meio. Use o |"
"mouseleft| e arraste para inclinar a forma."

#: ../../reference_manual/tools/shape_selection.rst:62
msgid "Tool Options"
msgstr "Opções da Ferramenta"

#: ../../reference_manual/tools/shape_selection.rst:64
msgid ""
"The tool options of this menu are quite involved, and separated over 3 tabs."
msgstr ""
"As opções das ferramentas deste menu estão intrinsecamente ligadas e estão "
"separadas em 3 páginas."

#: ../../reference_manual/tools/shape_selection.rst:68
msgid "Geometry"
msgstr "Geometria"

#: ../../reference_manual/tools/shape_selection.rst:74
msgid ""
"Geometry is the first section in the tool options. This section allows you "
"to set precisely the 'x' and 'y' coordinates, and also the width and height "
"of the shape."
msgstr ""
"A Geometria é a primeira secção nas opções de ferramentas. A mesma permite-"
"lhe definir de forma precisa o X, o Y, assim como a largura e a altura da "
"forma."

#: ../../reference_manual/tools/shape_selection.rst:77
msgid "Enabled: when scaling, it will scale the stroke width with the shape."
msgstr ""
"Activada: quando mudar a escala, irá ajustar a escala da espessura do traço "
"com a forma."

#: ../../reference_manual/tools/shape_selection.rst:78
msgid "Uniform scaling"
msgstr "Escala uniforme"

#: ../../reference_manual/tools/shape_selection.rst:79
msgid "Not enabled: when scaling, the stroke width will stay the same."
msgstr "Não activada: quando ajustar a escala, o traço continuará igual."

#: ../../reference_manual/tools/shape_selection.rst:80
msgid "Global coordinates"
msgstr "Coordenadas globais"

#: ../../reference_manual/tools/shape_selection.rst:81
msgid ""
"Determines whether the width and height bars use the width and height of the "
"object, while taking transforms into account."
msgstr ""
"Define se as barras de altura e largura usam as dimensões do objecto quando "
"tiverem as transformações em conta."

#: ../../reference_manual/tools/shape_selection.rst:83
#: ../../reference_manual/tools/shape_selection.rst:145
msgid "Opacity"
msgstr "Opacidade"

#: ../../reference_manual/tools/shape_selection.rst:83
msgid ""
"The general opacity, or transparency, of the object. Opacity for stroke and "
"fill are explained in the next two sections."
msgstr ""
"A opacidade ou transparência geral do objecto. A opacidade para o traço e o "
"preenchimento é explicada nas duas seguintes secções."

#: ../../reference_manual/tools/shape_selection.rst:87
msgid "Anchor Lock is not implemented at the moment."
msgstr "O Bloqueio da Âncora não está implementado de momento."

#: ../../reference_manual/tools/shape_selection.rst:91
msgid "Stroke"
msgstr "Traço"

#: ../../reference_manual/tools/shape_selection.rst:97
msgid "The stroke tab determines how the stroke around the object should look."
msgstr ""
"A página do Traço define como é deverá ficar o visual do traço em torno do "
"objecto."

#: ../../reference_manual/tools/shape_selection.rst:99
msgid ""
"The first set of buttons allows us to set the fill of the stroke: *None*, "
"*Color* and *Gradient*; the same options exist for the fill of the shape, "
"please refer to the following \"**Fill**\" section for more details on how "
"to use both of them."
msgstr ""
"O primeiro conjunto de botões a escolher é o preenchimento do traço: "
"*Nenhum*, *Cor* e *Gradiente*. Isto tem as mesmas funcionalidades que o "
"preenchimento da forma; por favor consulte a seguinte secção "
"**Preenchimento** para saber como usar ambos."

#: ../../reference_manual/tools/shape_selection.rst:101
msgid "Then, there are the settings for the stroke style:"
msgstr "Depois, existe a configuração do estilo do traço:"

#: ../../reference_manual/tools/shape_selection.rst:103
msgid "Thickness"
msgstr "Espessura"

#: ../../reference_manual/tools/shape_selection.rst:104
msgid ""
"Sets the width of the stroke. When creating a shape, Krita will use the "
"current brush size to determine the width of the stroke."
msgstr ""
"Define a espessura do traço. Ao criar uma forma, o Krita irá usar o tamanho "
"actual do pincel para definir a espessura do traço."

#: ../../reference_manual/tools/shape_selection.rst:105
msgid "Cap and corner style"
msgstr "Estilo dos cantos e extremos"

#: ../../reference_manual/tools/shape_selection.rst:106
msgid ""
"Sets the stroke cap and stroke corner style, this can be accessed by "
"pressing the three dots button next to the thickness entry."
msgstr ""
"Define o estilo de extremidades e de cantos do traço. Isto poderá ser "
"acedido se carregar no botão das reticências a seguir ao elemento da "
"espessura."

#: ../../reference_manual/tools/shape_selection.rst:107
msgid "Line-style"
msgstr "Estilo da linha"

#: ../../reference_manual/tools/shape_selection.rst:108
msgid ""
"Sets the line style of the stroke: *solid*, *dashes*, *dots*, or mixes of "
"*dashes and dots*."
msgstr ""
"Define o estilo da linha do traço: *ininterrupto*, *tracejado*, *pontilhado* "
"ou misto com *traços e pontos*."

#: ../../reference_manual/tools/shape_selection.rst:110
msgid "Markers"
msgstr "Marcadores"

#: ../../reference_manual/tools/shape_selection.rst:110
msgid ""
"Adds markers to the stroke. Markers are little figures that will appear at "
"the start, end or all the nodes in between, depending on your configuration."
msgstr ""
"Adiciona marcadores ao traço. Os marcadores são pequenas imagens que irão "
"aparecer no início, no fim ou em todos os nós intermédios, dependendo da sua "
"configuração."

#: ../../reference_manual/tools/shape_selection.rst:113
msgid "Fill"
msgstr "Preenchimento"

#: ../../reference_manual/tools/shape_selection.rst:118
msgid ""
"This section is about the color that fills the shape. As mentioned above in "
"the **Stroke** section, the features are the same for both the fill of the "
"stroke and the fill of the shape. Here is the explanation for both:"
msgstr ""
"Esta secção diz respeito à cor que preenche a forma. Como foi mencionado "
"acima na secção do **Traço**, as funcionalidades são as mesmas tanto para o "
"preenchimento do traço como para o preenchimento da forma. Aqui está a "
"explicação para ambos:"

#: ../../reference_manual/tools/shape_selection.rst:120
msgid "A fill can be: *solid color*, *gradient*, or *none* (transparent)"
msgstr ""
"O preenchimento poderá ser: *cor única*, um *gradiente* ou *nenhum* "
"(transparente)"

#: ../../reference_manual/tools/shape_selection.rst:122
msgid "None"
msgstr "Nenhum"

#: ../../reference_manual/tools/shape_selection.rst:123
msgid "No fill. It's transparent."
msgstr "Sem preenchimento. Fica transparente."

#: ../../reference_manual/tools/shape_selection.rst:124
msgid "Color"
msgstr "Cor"

#: ../../reference_manual/tools/shape_selection.rst:125
msgid "A flat color, you can select a new one by pressing the color button."
msgstr ""
"Uma única cor. Poderá seleccionar uma nova se carregar no botão de cores."

#: ../../reference_manual/tools/shape_selection.rst:127
msgid ""
"As the name implies this type fills the shape with a gradient. It has the "
"following options:"
msgstr ""
"Como o nome indica, este tipo preenche a forma com um gradiente. Tem as "
"seguintes opções:"

#: ../../reference_manual/tools/shape_selection.rst:129
msgid "Type"
msgstr "Tipo"

#: ../../reference_manual/tools/shape_selection.rst:130
msgid "A linear or radial gradient."
msgstr "Um gradiente linear ou radial."

#: ../../reference_manual/tools/shape_selection.rst:131
msgid "Repeat"
msgstr "Repetir"

#: ../../reference_manual/tools/shape_selection.rst:132
msgid "How the gradient repeats itself."
msgstr "Como é que se repete o gradiente."

#: ../../reference_manual/tools/shape_selection.rst:133
msgid "Preset"
msgstr "Predefinição"

#: ../../reference_manual/tools/shape_selection.rst:134
msgid ""
"A menu for selecting a base gradient from a set of predefined gradient "
"presets, which can be edited as desired."
msgstr ""
"Um menu para seleccionar um gradiente de base a partir de um conjunto de "
"predefinições de gradientes, os quais pode editar como desejar."

#: ../../reference_manual/tools/shape_selection.rst:135
msgid "Save Gradient"
msgstr "Gravar o Gradiente"

#: ../../reference_manual/tools/shape_selection.rst:136
msgid "A quick way for saving the current gradient as a preset."
msgstr "Uma forma rápida de gravar o gradiente actual como uma predefinição."

#: ../../reference_manual/tools/shape_selection.rst:138
msgid "Stops Options Line"
msgstr "Linhas Opções das Paragens"

#: ../../reference_manual/tools/shape_selection.rst:138
msgid ""
"A representation of how the gradient colors should look. The stops are "
"represented by triangles. There are two stops by default one at the "
"beginning and one at the end. You can create more stops just by clicking "
"anywhere on the line. To select a stop |mouseleft| inside the triangle. To "
"delete the stops, |mouseleft| drag them to left or right until the end of "
"the line."
msgstr ""
"Uma representação da forma como deverão aparecer as cores dos gradientes. As "
"paragens são representadas por triângulos. Existem duas paragens por "
"omissão, estando uma no início e outra no fim, Poderá criar mais paragens se "
"carregar em qualquer ponto da linha. Para seleccionar uma paragem, use o |"
"mouseleft| dentro do triângulo. Para apagar as paragens, arraste-a com o |"
"mouseleft| para a esquerda ou para a direita em direcção ao extremo "
"respectivo."

#: ../../reference_manual/tools/shape_selection.rst:140
msgid "Flip Gradient"
msgstr "Inverter o Gradiente"

#: ../../reference_manual/tools/shape_selection.rst:141
msgid "A quick way to invert the order of the gradient."
msgstr "Uma forma rápida de inverter a ordem do gradiente."

#: ../../reference_manual/tools/shape_selection.rst:142
msgid "Stop"
msgstr "Parar"

#: ../../reference_manual/tools/shape_selection.rst:143
msgid "Choose a color for the current selected stop."
msgstr "Escolha uma cor para a paragem seleccionada de momento."

#: ../../reference_manual/tools/shape_selection.rst:145
msgid "Gradient"
msgstr "Gradiente"

#: ../../reference_manual/tools/shape_selection.rst:145
msgid "Choose the opacity for the current selected stop."
msgstr "Escolha a opacidade para a paragem seleccionada de momento."

#: ../../reference_manual/tools/shape_selection.rst:149
msgid ""
"When a stop triangle is selected, it is highlighted with a slight blue "
"outline. The selected stop triangle will change its color and opacity "
"accordingly when these options are changed."
msgstr ""
"Quando seleccionar um triângulo de paragem, o mesmo fica realçado com um "
"ligeiro contorno azul. O triângulo da paragem seleccionada irá mudar a sua "
"cor e opacidade de acordo com as alterações destas opções."

#: ../../reference_manual/tools/shape_selection.rst:153
msgid ""
"You can edit the gradient in two ways. The first one is the actual gradient "
"in the docker that you can manipulate. Vectors always use stop-gradients. "
"The other way to edit gradients is editing their position on the canvas."
msgstr ""
"Poderá editar o gradiente de duas formas. A primeira é o gradiente actual na "
"área acoplável, que poderá manipular. Os vectores usam sempre gradientes com "
"paragens. A outra forma de editar os gradientes é editar a posição dos "
"mesmos na área de desenho."

#: ../../reference_manual/tools/shape_selection.rst:157
msgid "Right-click menu"
msgstr "Menu do botão direito"

#: ../../reference_manual/tools/shape_selection.rst:159
msgid ""
"The shape selection tool has a nice right click menu that gives you several "
"features. If you have an object selected, you can perform various functions "
"like cutting, copying, or moving the object to the front or back."
msgstr ""
"A ferramenta de selecção de formas tem um menu de botão direito agradável "
"que lhe oferece diversas funcionalidades. Se tiver um objecto seleccionado, "
"poderá efectuar várias funções, como o corte, a cópia ou a passagem do "
"objecto para a frente ou para trás."

#: ../../reference_manual/tools/shape_selection.rst:162
msgid ".. image:: images/vector/Vector-right-click-menu.png"
msgstr ".. image:: images/vector/Vector-right-click-menu.png"

#: ../../reference_manual/tools/shape_selection.rst:163
msgid ""
"If you have multiple objects selected you can perform \"Logical Operators\" "
"on them, or boolean operations as they are commonly called. It will be the "
"last item on the right-click menu. You can unite, intersect, subtract, or "
"split the selected objects."
msgstr ""
"Se tiver vários objectos seleccionados, poderá aplicar \"Operações Lógicas"
"\" (ou booleanas, como também são chamadas) sobre os mesmos. Será o último "
"item no menu do botão direito. Poderá combinar, subtrair, interceptar ou "
"dividir os objectos."
