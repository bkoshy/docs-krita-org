# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-08 17:30+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: Task set image en images dockers\n"

#: ../../reference_manual/dockers/task_sets.rst:1
msgid "Overview of the task sets docker."
msgstr "Introdução à área de conjuntos de tarefas."

#: ../../reference_manual/dockers/task_sets.rst:16
msgid "Task Sets Docker"
msgstr "Área de Conjuntos de Tarefas"

#: ../../reference_manual/dockers/task_sets.rst:18
msgid ""
"Task sets are for sharing a set of steps, like a tutorial. You make them "
"with the task-set docker."
msgstr ""
"Os conjuntos de tarefas servem para partilhar um conjunto de passos, como se "
"fosse um tutorial. Podê-los-á compor com a área de conjuntos de tarefas."

#: ../../reference_manual/dockers/task_sets.rst:21
msgid ".. image:: images/dockers/Task-set.png"
msgstr ".. image:: images/dockers/Task-set.png"

#: ../../reference_manual/dockers/task_sets.rst:22
msgid ""
"Task sets can record any kind of command also available via the shortcut "
"manager. It can not record strokes, like the macro recorder can. However, "
"you can play macros with the tasksets!"
msgstr ""
"Os conjuntos de tarefas conseguem gravar qualquer tipo de comandos que "
"estejam também disponíveis no gestor de atalhos. Não consegue gravar traços, "
"como acontece com o gravador de macros. Contudo, poderá reproduzir macros "
"com os conjuntos de tarefas!"

#: ../../reference_manual/dockers/task_sets.rst:24
msgid ""
"The tasksets docker has a record button, and you can use this to record a "
"certain workflow. Then use this to let items appear in the taskset list. "
"Afterwards, turn off the record. You can then click any action in the list "
"to make them happen. Press the 'Save' icon to name and save the taskset."
msgstr ""
"A área de conjuntos de tarefas tem um botão de gravação, o qual poderá ser "
"usado para gravar um dado fluxo de trabalho. Depois, pode usar isto para "
"apresentar os itens na lista de conjuntos de tarefas. Depois disso tudo, "
"desligue a gravação. Poderá então carregar em qualquer acção da lista para a "
"executar. Carregue no botão 'Gravar' para atribuir um nome e gravar o "
"conjunto de tarefas."
