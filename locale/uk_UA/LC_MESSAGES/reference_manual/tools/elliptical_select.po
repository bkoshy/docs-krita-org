# Translation of docs_krita_org_reference_manual___tools___elliptical_select.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___tools___elliptical_select\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 11:36+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../<generated>:1
msgid "Ratio"
msgstr "Пропорція"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"

#: ../../<rst_epilog>:68
msgid ""
".. image:: images/icons/elliptical_select_tool.svg\n"
"   :alt: toolselectellipse"
msgstr ""
".. image:: images/icons/elliptical_select_tool.svg\n"
"   :alt: toolselectellipse"

#: ../../reference_manual/tools/elliptical_select.rst:1
msgid "Krita's elliptical selector tool reference."
msgstr "Довідник із засобу еліптичного позначення Krita."

#: ../../reference_manual/tools/elliptical_select.rst:11
msgid "Tools"
msgstr "Інструменти"

#: ../../reference_manual/tools/elliptical_select.rst:11
msgid "Ellipse"
msgstr "Еліпс"

#: ../../reference_manual/tools/elliptical_select.rst:11
msgid "Circle"
msgstr "Коло"

#: ../../reference_manual/tools/elliptical_select.rst:11
msgid "Elliptical Select"
msgstr "Еліптичний вибір"

#: ../../reference_manual/tools/elliptical_select.rst:11
msgid "Selection"
msgstr "Позначення"

#: ../../reference_manual/tools/elliptical_select.rst:16
msgid "Elliptical Selection Tool"
msgstr "Інструмент «Еліптичне позначення»"

#: ../../reference_manual/tools/elliptical_select.rst:18
msgid "|toolselectellipse|"
msgstr "|toolselectellipse|"

#: ../../reference_manual/tools/elliptical_select.rst:20
msgid ""
"This tool, represented by an ellipse with a dashed border, allows you to "
"make :ref:`selections_basics` of a elliptical area. Simply click and drag "
"around the section you wish to select."
msgstr ""
"За допомогою цього інструмента, на піктограмі якого зображено еліпс із "
"пунктирною лінією, ви можете створювати :ref:`selections_basics` у формі "
"еліпса. Просто натисніть |mouseleft| і перетягніть вказівник уздовж ділянки, "
"яку ви хочете позначити."

#: ../../reference_manual/tools/elliptical_select.rst:23
msgid "Hotkeys and Stickykeys"
msgstr "Клавіші керування"

#: ../../reference_manual/tools/elliptical_select.rst:25
msgid ":kbd:`J` selects this tool."
msgstr ":kbd:`J` — вибрати цей інструмент."

#: ../../reference_manual/tools/elliptical_select.rst:26
msgid ""
":kbd:`R` sets the selection to 'replace' in the tool options, this is the "
"default mode."
msgstr ""
":kbd:`R` встановлює параметри позначення у режим «замінити»; це типовий "
"режим."

#: ../../reference_manual/tools/elliptical_select.rst:27
msgid ":kbd:`A` sets the selection to 'add' in the tool options."
msgstr ""
":kbd:`A` встановлює для позначення режим «додавання» у параметрах "
"інструмента."

#: ../../reference_manual/tools/elliptical_select.rst:28
msgid ":kbd:`S` sets the selection to 'subtract' in the tool options."
msgstr ""
":kbd:`S` встановлює для позначення режим «віднімання» у параметрах "
"інструмента."

#: ../../reference_manual/tools/elliptical_select.rst:29
msgid ""
":kbd:`Shift` after starting the selection, constraints it to a perfect "
"circle."
msgstr ""
"Натискання :kbd:`Shift` після того, як буде розпочато позначення, обмежує "
"область позначення ідеальним колом."

#: ../../reference_manual/tools/elliptical_select.rst:30
msgid ""
":kbd:`Ctrl` after starting the selection, makes the selection resize from "
"center."
msgstr ""
"Натискання :kbd:`Ctrl` після того, як буде розпочато позначення, надає змогу "
"змінювати розмір позначеного від центру."

#: ../../reference_manual/tools/elliptical_select.rst:31
msgid ":kbd:`Alt` after starting the selection, allows you to move it."
msgstr ""
"Натискання :kbd:`Alt` після того, як буде розпочато позначення, надає змогу "
"його пересувати."

#: ../../reference_manual/tools/elliptical_select.rst:32
msgid ""
":kbd:`Shift +` |mouseleft| sets the subsequent selection to 'add'. You can "
"release the :kbd:`Shift` key while dragging, but it will still be set to "
"'add'. Same for the others."
msgstr ""
"За допомогою комбінації :kbd:`Shift` + |mouseleft| можна встановити для "
"наступного позначення області режим «додавання». Під час перетягування ви "
"можете відпустити клавішу :kbd:`Shift`, але режим лишатиметься режимом "
"«додавання». Те саме стосується і інших режимів."

#: ../../reference_manual/tools/elliptical_select.rst:33
msgid ":kbd:`Alt +` |mouseleft| sets the subsequent selection to 'subtract'."
msgstr ""
":kbd:`Alt` + |mouseleft| встановлює для наступного позначення режим "
"«віднімання»."

#: ../../reference_manual/tools/elliptical_select.rst:34
msgid ":kbd:`Ctrl +` |mouseleft| sets the subsequent selection to 'replace'."
msgstr ""
":kbd:`Ctrl` + |mouseleft| встановлює для наступного позначення режим "
"«заміна»."

#: ../../reference_manual/tools/elliptical_select.rst:35
msgid ""
":kbd:`Shift + Alt +` |mouseleft| sets the subsequent selection to  "
"'intersect'."
msgstr ""
":kbd:`Shift + Alt +` |mouseleft| встановлює для наступного позначення режим "
"«перетин»."

#: ../../reference_manual/tools/elliptical_select.rst:39
msgid "Hovering over a selection allows you to move it."
msgstr ""
"Наведення вказівника на позначену ділянку надасть вам змогу пересунути її."

#: ../../reference_manual/tools/elliptical_select.rst:40
msgid ""
"|mouseright| will open up a selection quick menu with amongst others the "
"ability to edit the selection."
msgstr ""
"Клацання |mouseright| відкриває меню швидкого доступу для позначеної "
"ділянки, у якому, серед інших пунктів, є і пункт редагування позначеної "
"ділянки."

#: ../../reference_manual/tools/elliptical_select.rst:44
msgid ""
"So to subtract a perfect circle, you do :kbd:`Alt +` |mouseleft|, then "
"release the :kbd:`Alt` key while dragging and press the :kbd:`Shift` key to "
"constrain."
msgstr ""
"Отже, щоб відняти від позначеної ділянки коло, вам слід скористатися "
"комбінацією :kbd:`Alt +` |mouseleft|, потім відпустити клавішу :kbd:`Alt` "
"під час перетягування вказівника і натиснути клавішу :kbd:`Shift` для "
"встановлення режиму."

#: ../../reference_manual/tools/elliptical_select.rst:49
msgid ""
"You can switch the behavior of the :kbd:`Alt` key to use the :kbd:`Ctrl` key "
"instead by toggling the switch in the :ref:`general_settings`."
msgstr ""
"Ви можете перемкнути поведінку програми у відповідь на натискання :kbd:`Alt` "
"так, щоб натискати замість цієї клавіші клавішу :kbd:`Ctrl`. Для цього "
"призначено спеціальний пункт на сторінці :ref:`general_settings`."

#: ../../reference_manual/tools/elliptical_select.rst:52
msgid "Tool Options"
msgstr "Параметри інструмента"

#: ../../reference_manual/tools/elliptical_select.rst:54
msgid "Anti-aliasing"
msgstr "Згладжування"

#: ../../reference_manual/tools/elliptical_select.rst:55
msgid ""
"This toggles whether or not to give selections feathered edges. Some people "
"prefer hard-jagged edges for their selections."
msgstr ""
"Цей пункт визначає, чи буде програма згладжувати межі позначеної ділянки. "
"Дехто надає перевагу різким межам позначених ділянок."

#: ../../reference_manual/tools/elliptical_select.rst:56
msgid "Width"
msgstr "Товщина"

#: ../../reference_manual/tools/elliptical_select.rst:57
msgid ""
"Gives the current width. Use the lock to force the next selection made to "
"this width."
msgstr ""
"Задає поточну ширину. Скористайтеся кнопкою із замочком, щоб примусово "
"задати для наступного позначення цю ширину."

#: ../../reference_manual/tools/elliptical_select.rst:58
msgid "Height"
msgstr "Висота"

#: ../../reference_manual/tools/elliptical_select.rst:59
msgid ""
"Gives the current height. Use the lock to force the next selection made to "
"this height."
msgstr ""
"Задає поточну висоту. Скористайтеся кнопкою із замочком, щоб примусово "
"задати для наступного позначення цю висоту."

#: ../../reference_manual/tools/elliptical_select.rst:61
msgid ""
"Gives the current ratio. Use the lock to force the next selection made to "
"this ratio."
msgstr ""
"Задає поточне співвідношення розмірів. Скористайтеся кнопкою із замочком, "
"щоб примусово задати для наступного позначення це співвідношення розміру."
