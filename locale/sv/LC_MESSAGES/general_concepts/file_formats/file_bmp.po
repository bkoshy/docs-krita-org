# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-19 03:24+0200\n"
"PO-Revision-Date: 2019-07-30 20:30+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../general_concepts/file_formats/file_bmp.rst:1
msgid "The Bitmap file format."
msgstr "Filformatet bitavbildning."

#: ../../general_concepts/file_formats/file_bmp.rst:10
msgid "*.bmp"
msgstr "*.bmp"

#: ../../general_concepts/file_formats/file_bmp.rst:10
msgid "BMP"
msgstr "BMP"

#: ../../general_concepts/file_formats/file_bmp.rst:10
msgid "Bitmap Fileformat"
msgstr "Filformatet bitavbildning"

#: ../../general_concepts/file_formats/file_bmp.rst:15
msgid "\\*.bmp"
msgstr "\\*.bmp"

#: ../../general_concepts/file_formats/file_bmp.rst:17
msgid ""
"``.bmp``, or Bitmap, is the simplest raster file format out there, and, "
"being patent-free, most programs can open and save bitmap files."
msgstr ""
"Bitavbildning eller ``.bmp`` är det enklaste rastreringsfilformatet som är "
"tillgängligt, och eftersom det är patentfritt kan de flesta program öppna "
"och spara bitavbildningsfiler."

#: ../../general_concepts/file_formats/file_bmp.rst:19
msgid ""
"However, most programs don't compress bitmap files, leading to BMP having a "
"reputation for being very heavy. If you need a lossless file format, we "
"actually recommend :ref:`file_png`."
msgstr ""
"Dock komprimerar inte de flesta program bitavbildningsfiler, vilket leder "
"till att BMP har ryktet att vara mycket omfattande. Om du behöver ett "
"förlustfritt filformat, rekommenderar vi hellre :ref:`file_png`."
