msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-08-16 17:04\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/docs_krita_org_general_concepts.pot\n"

#: ../../general_concepts.rst:5
msgid "General Concepts"
msgstr "一般概念"

#: ../../general_concepts.rst:7
msgid ""
"Learn about general art and technology concepts that are not specific to "
"Krita."
msgstr "了解非 Krita 特有的一般美术和技术概念。"

#: ../../general_concepts.rst:9
msgid "Contents:"
msgstr "目录："
