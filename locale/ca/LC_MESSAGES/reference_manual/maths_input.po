# Translation of docs_krita_org_reference_manual___maths_input.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-13 17:32+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.1\n"

#: ../../reference_manual/maths_input.rst:1
msgid ""
"Overview of maths operations that can be used in Krita spinboxes and number "
"inputs."
msgstr ""
"Resum de les operacions de matemàtiques que es poden utilitzar als botons de "
"selecció de valors del Krita i a les entrades de números."

#: ../../reference_manual/maths_input.rst:12
msgid "Maths"
msgstr "Matemàtiques"

#: ../../reference_manual/maths_input.rst:17
msgid "Maths Input"
msgstr "Entrada de les matemàtiques"

#: ../../reference_manual/maths_input.rst:19
msgid ""
"Also known as Numerical Input boxes. You can make Krita do simple maths for "
"you in the places where we have number input. Just select the number in a "
"spinbox, or right-click a slider to activate number input. It doesn't do "
"unit conversion yet, but this is planned."
msgstr ""
"També coneguts com a quadres d'entrada numèrica. Podeu fer que el Krita faci "
"matemàtiques simples en els llocs on tenim entrada de números. Simplement "
"seleccioneu el número en un botó de selecció de valors o feu clic dret en un "
"control lliscant per activar l'entrada del número. Encara no fa la conversió "
"d'unitats, però està planejat."

#: ../../reference_manual/maths_input.rst:22
msgid "Possible Functions"
msgstr "Funcions possibles"

# skip-rule: t-acc_obe
#: ../../reference_manual/maths_input.rst:25
msgid "Just adds the numbers. Usage: ``50+100`` Output: ``150``"
msgstr "Simplement suma els números. Ús: ``50+100``. Sortida: ``150``."

#: ../../reference_manual/maths_input.rst:26
msgid "Addition (Operator: + )"
msgstr "Addició (operador: + )"

# skip-rule: t-acc_obe
#: ../../reference_manual/maths_input.rst:29
msgid ""
"Just subtracts the last number from the first. Usage: ``50-100`` Output: "
"``50``"
msgstr ""
"Simplement resta el darrer número del primer. Ús: ``50-100``. Sortida: "
"``50``."

#: ../../reference_manual/maths_input.rst:30
msgid "Subtraction (Operator: - )"
msgstr "Sostracció (operador: - )"

# skip-rule: t-acc_obe
#: ../../reference_manual/maths_input.rst:33
msgid "Just multiplies the numbers. Usage: ``50*100`` Output: ``5000``"
msgstr "Simplement multiplica els números. Ús: ``50*100``. Sortida: ``5000``."

#: ../../reference_manual/maths_input.rst:34
msgid "Multiplication (Operator: * )"
msgstr "Multiplicació (operador: * )"

# skip-rule: t-acc_obe
#: ../../reference_manual/maths_input.rst:37
msgid "Just divides the numbers. Usage: ``50/100`` Output: ``0.5``"
msgstr "Simplement divideix els números. Ús: ``50/100``. Sortida: ``0,5``."

#: ../../reference_manual/maths_input.rst:38
msgid "Division (Operator: / )"
msgstr "Divisió (operador: / )"

# skip-rule: t-acc_obe
#: ../../reference_manual/maths_input.rst:41
msgid ""
"Makes the last number the exponent of the first and calculates the result. "
"Usage: ``2^8`` Output: ``256``"
msgstr ""
"Fa que el darrer número sigui l'exponent del primer i calcula el resultat. "
"Ús: ``2^8``. Sortida: ``256``."

#: ../../reference_manual/maths_input.rst:42
msgid "Exponent (Operator: ^ )"
msgstr "Exponent (operador: ^ )"

# skip-rule: t-acc_obe
#: ../../reference_manual/maths_input.rst:45
msgid ""
"Gives you the sine of the given angle. Usage: ``sin(50)`` Output: ``0.76``"
msgstr "Dóna el sinus de l'angle indicat. Ús: ``sin(50)``. Sortida: ``0,76``."

#: ../../reference_manual/maths_input.rst:46
msgid "Sine (Operator: sin() )"
msgstr "Sinus (operador: sin() )"

# skip-rule: t-acc_obe
#: ../../reference_manual/maths_input.rst:49
msgid ""
"Gives you the cosine of the given angle. Usage: ``cos(50)`` Output: ``0.64``"
msgstr ""
"Dóna el cosinus de l'angle indicat. Ús: ``cos(50)``. Sortida: ``0,64``."

#: ../../reference_manual/maths_input.rst:50
msgid "Cosine (Operator: cos() )"
msgstr "Cosinus (operador: cos() )"

# skip-rule: t-acc_obe
#: ../../reference_manual/maths_input.rst:53
msgid ""
"Gives you the tangent of the given angle. Usage: ``tan(50)`` Output: ``1.19``"
msgstr ""
"Dóna la tangent de l'angle indicat. Ús: ``tan(50)``. Sortida: ``1,19``."

#: ../../reference_manual/maths_input.rst:54
msgid "Tangent (Operator: tan() )"
msgstr "Tangent (operador: tan() )"

# skip-rule: t-acc_obe
#: ../../reference_manual/maths_input.rst:57
msgid ""
"Inverse function of the sine, gives you the angle which the sine equals the "
"argument. Usage: ``asin(0.76)`` Output: ``50``"
msgstr ""
"La funció inversa del sinus, dóna l'angle amb el qual el sinus és igual que "
"l'argument. Ús: ``asin(0,76)``. Sortida: ``50``."

#: ../../reference_manual/maths_input.rst:58
msgid "Arc Sine (Operator: asin() )"
msgstr "Arc sinus (operador: asin() )"

# skip-rule: t-acc_obe
#: ../../reference_manual/maths_input.rst:61
msgid ""
"Inverse function of the cosine, gives you the angle which the cosine equals "
"the argument. Usage: ``acos(0.64)`` Output: ``50``"
msgstr ""
"La funció inversa del cosinus, dóna l'angle amb el qual el cosinus és igual "
"que l'argument. Ús: ``acos(0,64)``. Sortida: ``50``."

#: ../../reference_manual/maths_input.rst:62
msgid "Arc Cosine (Operator: acos() )"
msgstr "Arc cosinus (operador: acos() )"

# skip-rule: t-acc_obe
#: ../../reference_manual/maths_input.rst:65
msgid ""
"Inverse function of the tangent, gives you the angle which the tangent "
"equals the argument. Usage: ``atan(1.19)`` Output: ``50``"
msgstr ""
"La funció inversa de la tangent, dóna l'angle amb el qual la tangent és "
"igual que l'argument. Ús: ``atan(1,19)``. Sortida: ``50``."

#: ../../reference_manual/maths_input.rst:66
msgid "Arc Tangent (Operator: atan() )"
msgstr "Arc tangent (operador: atan() )"

# skip-rule: t-acc_obe
#: ../../reference_manual/maths_input.rst:69
msgid ""
"Gives you the value without negatives. Usage: ``abs(75-100)`` Output: ``25``"
msgstr "Dóna el valor sense negatius. Ús: ``abs(75-100)``. Sortida: ``25``."

#: ../../reference_manual/maths_input.rst:70
msgid "Absolute (Operator: abs() )"
msgstr "Absolut (operador: abs() )"

# skip-rule: t-acc_obe
#: ../../reference_manual/maths_input.rst:73
msgid ""
"Gives you given values using e as the exponent. Usage: ``exp(1)`` Output: "
"``2.7183``"
msgstr ""
"Dóna els valors indicats emprant «e» com a l'exponent. Ús: ``exp(1)``. "
"Sortida: ``2,7183``."

#: ../../reference_manual/maths_input.rst:74
msgid "Exponent (Operator: exp() )"
msgstr "Exponent (operador: exp() )"

# skip-rule: t-acc_obe
#: ../../reference_manual/maths_input.rst:77
msgid ""
"Gives you the natural logarithm, which means it has the inverse "
"functionality to exp(). Usage: ``ln(2)`` Output: ``0.6931``"
msgstr ""
"Dóna el logaritme natural, el qual vol dir que té la funcionalitat inversa a "
"exp(). Ús: ``ln(2)``. Sortida: ``0,6931``."

#: ../../reference_manual/maths_input.rst:79
msgid "Natural Logarithm (Operator: ln() )"
msgstr "Logaritme natural (operador: ln() )"

#: ../../reference_manual/maths_input.rst:81
msgid "The following are technically supported but bugged:"
msgstr "La següent està tècnicament admesa però amb errors:"

# skip-rule: t-acc_obe
#: ../../reference_manual/maths_input.rst:84
msgid ""
"Gives you logarithms of the given value. Usage: ``log10(50)`` Output: "
"``0.64``"
msgstr ""
"Dóna logaritmes del valor indicat. Ús: ``log10(50)``. Sortida: ``0,64``."

#: ../../reference_manual/maths_input.rst:86
msgid "Common Logarithm (Operator: log10() )"
msgstr "Logaritme habitual (operador: log10() )"

# skip-rule: punctuation-period
#: ../../reference_manual/maths_input.rst:89
msgid "Order of Operations."
msgstr "Ordre de les operacions"

#: ../../reference_manual/maths_input.rst:91
msgid ""
"The order of operations is a globally agreed upon reading order for "
"interpreting mathematical expressions. It solves how to read an expression "
"like:"
msgstr ""
"L'ordre de les operacions és un ordre de lectura acordat globalment per a "
"interpretar les expressions matemàtiques. Resol com llegir una expressió com:"

# skip-rule: t-acc_obe
#: ../../reference_manual/maths_input.rst:93
msgid "``2+3*4``"
msgstr "``2+3*4``"

#: ../../reference_manual/maths_input.rst:95
msgid ""
"You could read it as 2+3 = 5, and then 5*4 =20. Or you could say 3*4 = 12 "
"and then 2+12 = 14."
msgstr ""
"Podeu llegir-la com 2+3 = 5, i després 5*4 = 20. O podríeu dir 3*4 = 12 i "
"després 2+12 = 14."

#: ../../reference_manual/maths_input.rst:97
msgid ""
"The order of operations itself is Exponents, Multiplication, Addition, "
"Subtraction. So we first multiply, and then add, making the answer to the "
"above 14, and this is how Krita will interpret the above."
msgstr ""
"L'ordre de les operacions en si és Exponents, Multiplicació, Suma i "
"Sostracció. De manera que primer multipliquem i després sumem, donant la "
"resposta a les 14 anteriors, i així és com el Krita interpretarà l'anterior."

#: ../../reference_manual/maths_input.rst:99
msgid ""
"We can use brackets to specify certain operations go first, so to get 20 "
"from the above expression, we do the following:"
msgstr ""
"Podem utilitzar parèntesis per a especificar que certes operacions van "
"primer, de manera que per obtenir 20 de l'expressió anterior, fem el següent:"

# skip-rule: t-acc_obe
#: ../../reference_manual/maths_input.rst:101
msgid "``( 2+3 )*4``"
msgstr "``( 2+3 )*4``"

#: ../../reference_manual/maths_input.rst:103
msgid ""
"Krita can interpret the brackets accordingly and will give 20 from this."
msgstr ""
"El Krita pot interpretar els parèntesis en conseqüència i el resultat d'això "
"donarà 20."

#: ../../reference_manual/maths_input.rst:106
msgid "Errors"
msgstr "Errors"

#: ../../reference_manual/maths_input.rst:108
msgid ""
"Sometimes, you see the result becoming red. This means you made a mistake "
"and Krita cannot parse your maths expression. Simply click the input box and "
"try again."
msgstr ""
"De vegades, veureu que el resultat es torna vermell. Això voldrà dir que heu "
"comès un error i que el Krita no pot analitzar la vostra expressió "
"matemàtica. Simplement feu clic al quadre d'entrada i torneu a intentar-ho."
