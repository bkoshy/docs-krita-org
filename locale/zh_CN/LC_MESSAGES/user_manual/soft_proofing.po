msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-16 17:04\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_user_manual___soft_proofing.pot\n"

#: ../../user_manual/soft_proofing.rst:0
msgid ".. image:: images/softproofing/Softproofing_adaptationstate.png"
msgstr ""

#: ../../user_manual/soft_proofing.rst:1
msgid "How to use softproofing in Krita."
msgstr "如何在 Krita 里进行软打样。"

#: ../../user_manual/soft_proofing.rst:11
msgid "Color"
msgstr ""

#: ../../user_manual/soft_proofing.rst:11
msgid "Softproofing"
msgstr ""

#: ../../user_manual/soft_proofing.rst:16
msgid "Soft Proofing"
msgstr "软打样"

#: ../../user_manual/soft_proofing.rst:18
msgid ""
"When we make an image in Krita, and print that out with a printer, the image "
"tends to look different. The colors are darker, or less dark than expected, "
"maybe the reds are more aggressive, maybe contrast is lost. For simple "
"documents, this isn’t much of a problem, but for professional prints, this "
"can be very sad, as it can change the look and feel of an image drastically."
msgstr ""
"如果我们需要打印 Krita 制作的图像，我们有时会发现屏幕上显示的效果跟打印出来的"
"效果不太一样。有时候颜色深了，有时候颜色浅了，有时候红色会更冲，有时候反差弱"
"化了。如果只是在打印一些简单的文档，这点小事没什么大不了的。然而到了专业印刷"
"场景里这就是印刷事故了，毕竟颜色的偏差可能会严重影响图像的视觉效果。"

#: ../../user_manual/soft_proofing.rst:20
msgid ""
"The reason this happens is simply because the printer uses a different color "
"model (CMYK) and it has often access to a lower range of colors (called a "
"gamut)."
msgstr ""
"为什么会发生偏色呢？这是因为显示器使用的 RGB 色彩模型和打印机的 CMYK 模型不"
"同，打印机能够使用的色域往往更小。"

#: ../../user_manual/soft_proofing.rst:22
msgid ""
"A naive person would suggest the following solution: do your work within the "
"CMYK color model! But there are three problems with that:"
msgstr ""
"有些人会天真地以为：那我们在 CMYK 色彩模型下面工作不就行了嘛！但这种想法其实"
"有三大问题："

#: ../../user_manual/soft_proofing.rst:24
msgid ""
"Painting in a CMYK space doesn’t guarantee that the colors will be the same "
"on your printer. For each combination of Ink, Paper and Printing device, the "
"resulting gamut of colors you can use is different. Which means that each of "
"these could have a different profile associated with them."
msgstr ""
"在 CMYK 空间下绘画无法保证颜色和打印机的完全一致。因为不同的油墨、纸张和印刷"
"设备的组合会产生无数的色域变化，每个不同组合都会有一个与之匹配的特性文件。"

#: ../../user_manual/soft_proofing.rst:25
msgid ""
"Furthermore, even if you have the profile and are working in the exact color "
"space that your printer can output, the CMYK color space is very irregular, "
"meaning that the color maths isn’t as nice as in other spaces. Blending "
"modes are different in CMYK as well."
msgstr ""
"即使你的工作特性文件和打印机的输出特性文件完全一致，CMYK 本身依然是一种难以预"
"测的色彩空间。色彩运算在 CMYK 空间下面的结果要比在其他空间中的要差，混色模式"
"的结果也跟其他空间下面的大不相同。"

#: ../../user_manual/soft_proofing.rst:26
msgid ""
"Finally, working in that specific CMYK space means that the image is stuck "
"to that space. If you are preparing your work for  different a CMYK profile, "
"due to the paper, printer or ink being different, you might have a bigger "
"gamut with more bright colors that you would like to take advantage of."
msgstr ""
"最后，在一个特定的 CMYK 空间下面工作意味着制作的文件会被限制在该空间狭窄的色"
"域里。如果你要为另一个不同的 CMYK 特性文件作印前准备，由于纸张、打印机和油墨"
"发生了变化，可以使用的色域说不定就变大了，可是你的旧文件却无法发挥新设备的潜"
"力。"

#: ../../user_manual/soft_proofing.rst:28
msgid ""
"So ideally, you would do the image in RGB, and use all your favorite RGB "
"tools, and let the computer do a conversion to a given CMYK space on the "
"fly, just for preview. This is possible, and is what we call ''Soft "
"Proofing''."
msgstr ""
"所以在理想状况下，你应该在 RGB 空间下面绘制图像，使用你习惯的 RGB 图像工具，"
"然后让计算机随时切换到某个 CMYK 空间下面对颜色进行校对。这种技术早已成为现"
"实，我们把它叫做“软打样”。"

#: ../../user_manual/soft_proofing.rst:34
msgid ".. image:: images/softproofing/Softproofing_regularsoftproof.png"
msgstr ""

#: ../../user_manual/soft_proofing.rst:34
msgid ""
"On the left, the original, on the right, a view where soft proofing is "
"turned on. The difference is subtle due to the lack of really bright colors, "
"but the soft proofed version is slightly less blueish in the whites of the "
"flowers and slightly less saturated in the greens of the leaves."
msgstr ""
"左边的是原图，右边的是一个软打样功能启用后的视图。两者的区别相当微妙，这是因"
"为原图并未使用特别明亮的颜色。但我们不难发现在右边的软打样视图里，花朵的白色"
"没有原图那么偏青蓝，而叶子的绿色也没有原图那么鲜艳。"

#: ../../user_manual/soft_proofing.rst:36
msgid ""
"You can toggle soft proofing on any image using the :kbd:`Ctrl + Y` "
"shortcut. Unlike other programs, this is per-view, so that you can look at "
"your image non-proofed and proofed, side by side. The settings are also per "
"image, and saved into the .kra file. You can set the proofing options in :"
"menuselection:`Image --> Image Properties --> Soft Proofing`."
msgstr ""

#: ../../user_manual/soft_proofing.rst:38
msgid "There you can set the following options:"
msgstr "软打样功能有下列选项："

#: ../../user_manual/soft_proofing.rst:40
msgid "Profile, Depth, Space"
msgstr "特性、深度、模型"

#: ../../user_manual/soft_proofing.rst:41
msgid ""
"Of these, only the profile is really important. This will serve as the "
"profile you are proofing to. In a professional print workflow, this profile "
"should be determined by the printing house."
msgstr ""
"这三个选项里只有“特性”一项是特别重要的。特性文件是软打样的目标文件。在专业打"
"印流程中，此特性文件应该由印刷公司决定。"

#: ../../user_manual/soft_proofing.rst:43
msgid ""
"Set the proofing Intent. It uses the same intents as the intents mentioned "
"in the :ref:`color managed workflow <color_managed_workflow>`."
msgstr ""
"设置软打样的再现意图。 :ref:`色彩管理流程 <color_managed_workflow>` 章节对不"
"同的再现意图进行了介绍。"

#: ../../user_manual/soft_proofing.rst:48
msgid "Intent"
msgstr "再现意图"

#: ../../user_manual/soft_proofing.rst:49
msgid ""
"Left: Soft proofed image with Adaptation state slider set to max. Right: "
"Soft proofed image with Adaptation State set to minimum"
msgstr ""
"左图：软打样的“适应状态”滑动条被拉到最大。右图：软打样的“适应状态”滑动条被拉"
"到最小。"

#: ../../user_manual/soft_proofing.rst:50
msgid "Adaptation State"
msgstr "适应状态"

#: ../../user_manual/soft_proofing.rst:51
msgid ""
"A feature which allows you to set whether :guilabel:`Absolute Colorimetric` "
"will make the white in the image screen-white during proofing (the slider "
"set to max), or whether it will use the white point of the profile (the "
"slider set to minimum). Often CMYK profiles have a different white as the "
"screen, or amongst one another due to the paper color being different."
msgstr ""
"此选项控制在 :guilabel:`绝对比色模式` 下面使用屏幕白作为画面白的比例。滑动条"
"拉到最大将完全使用屏幕白，拉到最小则完全使用特性文件指定的白。在通常情况下 "
"CMYK 特性文件规定的白和屏幕的不同，不同纸张的白也会不同。"

#: ../../user_manual/soft_proofing.rst:52
msgid "Black Point Compensation"
msgstr "黑点补偿"

#: ../../user_manual/soft_proofing.rst:53
msgid ""
"Set the black point compensation. Turning this off will crunch the shadow "
"values to the minimum the screen and the proofing profile can handle, while "
"turning this on will scale the black to the screen-range, showing you the "
"full range of grays in the image."
msgstr ""
"此选项控制黑点补偿的开关。取消勾选此选项将把图像暗部剪切到最接近软打样特性文"
"件的屏幕亮度最小值。勾选此项则按照屏幕亮度范围缩放黑的数值，从而显示出图像全"
"部的灰阶色调。"

#: ../../user_manual/soft_proofing.rst:55
msgid "Gamut Warning"
msgstr "色域警告"

#: ../../user_manual/soft_proofing.rst:55
msgid "Set the color of the out-of-gamut warning."
msgstr "设置超出色域警告色。"

#: ../../user_manual/soft_proofing.rst:57
msgid ""
"You can set the defaults that Krita uses in :menuselection:`Settings --> "
"Configure Krita --> Color Management`."
msgstr ""
"要设置默认警告色，点击菜单栏的 :menuselection:`设置 --> 配置 Krita --> 色彩管"
"理` 。"

#: ../../user_manual/soft_proofing.rst:59
msgid ""
"To configure this properly, it's recommended to make a test image to print "
"(and that is printed by a properly set-up printer) and compare against, and "
"then approximate in the proofing options how the image looks compared to the "
"real-life copy you have made."
msgstr ""
"为了确保选项配置正确，我们推荐把图像在安装正确的打印机上进行试印，然后把样张"
"跟屏幕软打样的效果进行比对，以决定是否进一步调整软打样的选项。"

#: ../../user_manual/soft_proofing.rst:62
msgid "Out of Gamut Warning"
msgstr "超出色域警告"

#: ../../user_manual/soft_proofing.rst:64
msgid ""
"The out of gamut warning, or gamut alarm, is an extra option on top of Soft-"
"Proofing: It allows you to see which colors are being clipped, by replacing "
"the resulting color with the set alarm color."
msgstr ""
"超出色域警告色，也叫做色域警告，是软打样的一个附加功能。它使用被指定的警告色"
"来标记那些无法被包含在软打样特性文件色域内的颜色。"

#: ../../user_manual/soft_proofing.rst:66
msgid ""
"This can be useful to determine where certain contrasts are being lost, and "
"to allow you to change it slowly to a less contrasted image."
msgstr "此功能有助于发现潜在的反差损失，让你可以有针对性地调低图像的色彩反差。"

#: ../../user_manual/soft_proofing.rst:72
msgid ".. image:: images/softproofing/Softproofing_gamutwarnings.png"
msgstr ""

#: ../../user_manual/soft_proofing.rst:72
msgid ""
"Left: View with original image, Right: View with soft proofing and gamut "
"warnings turned on. Krita will save the gamut warning color alongside the "
"proofing options into the Kra file, so pick a color that you think will "
"stand out for your current image."
msgstr ""
"左图：原图视图，右图：启用了色域警告色的软打样视图。Krita 会在 KRA 图像的软打"
"样选项中保存色域警告色。你可以为你的当前图像选择一种易于发现的警告色。"

#: ../../user_manual/soft_proofing.rst:74
msgid ""
"You can activate Gamut Warnings with the :kbd:`Ctrl + Shift + Y` shortcut, "
"but it needs soft proofing activated to work fully."
msgstr ""

#: ../../user_manual/soft_proofing.rst:77
msgid ""
"Soft Proofing doesn’t work properly in floating-point spaces, and attempting "
"to force it will cause incorrect gamut alarms. It is therefore disabled."
msgstr ""
"软打样在浮点空间下面无法正常工作，会造成错误的色域警告色显示。因此在浮点空间"
"下软打样会被禁用。"

#: ../../user_manual/soft_proofing.rst:80
msgid ""
"Gamut Warnings sometimes give odd warnings for linear profiles in the "
"shadows. This is a bug in LCMS, see `here <https://ninedegreesbelow.com/bug-"
"reports/soft-proofing-problems.html>`_ for more info."
msgstr ""
